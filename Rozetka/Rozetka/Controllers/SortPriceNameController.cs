﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Rozetka.Entities;

namespace Rozetka.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SortPriceNameController : Controller
    {
        private readonly EFContext _context;

        public SortPriceNameController(EFContext context)
        {
            _context = context;
        }

        // GET api/values
        [HttpGet("GetMessages/{id}")]
        public ContentResult Get(int id)
        {
            List<Laptop> resault = new List<Laptop>();
            //По спадданю (name) 
            if (id == 0)
            {
                List<Laptop> laptops = _context.Laptops.ToList();
                resault = laptops.OrderByDescending(o => o.Name).ToList();
                string json = JsonConvert.SerializeObject(resault);
                return Content(json, "application/json");
            }
            //По зростанню (name)
            if (id == 1)
            {
                List<Laptop> laptops = _context.Laptops.ToList();
                laptops.Sort();
                resault = laptops.OrderBy(o => o.Name).ToList();
                string json = JsonConvert.SerializeObject(resault);
                return Content(json, "application/json");
            }
            //По спаданню (price)
            if (id == 2)
            {
                List<Laptop> laptops = _context.Laptops.ToList();
                laptops.Sort();
                resault = laptops.OrderByDescending(o => o.Price).ToList();
                string json = JsonConvert.SerializeObject(resault);
                return Content(json, "application/json");
            }
            //По зростанню (price)
            if (id == 3)
            {
                List<Laptop> laptops =  _context.Laptops.ToList();
                laptops.Sort();
                resault = laptops.OrderBy(o => o.Price).ToList();
                string json = JsonConvert.SerializeObject(resault);
                return Content(json, "application/json");
            }
            else
            {
                return null;
            }
        }
        }
    }