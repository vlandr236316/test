﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rozetka.Entities;
using Rozetka.Models;

namespace Rozetka.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompController : ControllerBase
    {
        private readonly EFContext _context;
        public CompController(EFContext context)
        {
            _context = context;
        }
        [HttpGet("get/search/{search}")]
        public IActionResult Search(string search)
        {
            var notebooks = from m in _context.Laptops
                         select m;


            if (!String.IsNullOrEmpty(search))
            {
                notebooks = notebooks.Where(s => s.Name.Contains(search));
            }

            return Ok(notebooks);

        }






    }
}