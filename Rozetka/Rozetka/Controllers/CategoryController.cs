﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rozetka.Entities;
using Rozetka.Models;

namespace Rozetka.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly EFContext _context;
        public CategoryController(EFContext context)
        {
            _context = context;
        }
        [HttpGet("get")]
        public IActionResult GetCategories()
        {
            List<CategoryViewModel> categories = _context.Categories.Select(t => new CategoryViewModel()
            {
                Id = t.Id,
                Name = t.Name,
                Description = t.Description,
                Laptops = t.Laptops.Select(a => new LaptopViewModel()
                {
                    Name = a.Name,
                    Price = a.Price,
                    IsAvailable = a.IsAvailable,
                    Quantity = a.Quantity,
                    Description = a.Description,
                    Image = a.Image
                }).ToList()
            }).ToList();
            return Ok(categories);
        }
    }
}