﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Rozetka.Entities;
using Rozetka.Models;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;

namespace Rozetka.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LaptopController : ControllerBase
    {
        private readonly EFContext context;
        private readonly IHostingEnvironment appEnvironment;
        public LaptopController(EFContext ccontext, IHostingEnvironment appenv)
        {
            context = ccontext;
            appEnvironment = appenv;
        }


        [HttpGet("getLaptop")]
        public ContentResult GetProducts()
        {
            List<Laptop> product = context.Laptops.ToList();
            string json = JsonConvert.SerializeObject(product);
            return Content(json, "application/json");
        }

        [HttpPost("addLaptop")]
        public ContentResult AddLaptop([FromBody]LaptopViewModel model)
        {
            try
            {
                string path = string.Empty;
                if (model.Image != null)
                {
                    byte[] imagebyte = Convert.FromBase64String(model.Image);
                    using (MemoryStream stream = new MemoryStream(imagebyte, 0, imagebyte.Length))
                    {
                        path = Guid.NewGuid().ToString() + ".jpg";
                        Image productImage = Image.FromStream(stream);
                        productImage.Save(appEnvironment.WebRootPath + @"/Content/" + path, ImageFormat.Jpeg);
                    }
                }

                Laptop laptop = new Laptop()
                {
                    Name=model.Name,
                    Price=model.Price,
                    IsAvailable=model.IsAvailable,
                    Quantity=model.Quantity,
                    Description=model.Description,
                    Image=path
                };
                context.Laptops.Add(laptop);
                context.SaveChanges();
                return Content("Product successfuly added");
            }
            catch (Exception ex)
            {
                return Content("Error " + ex.Message);
            }
        }
        [HttpDelete("deleteLaptop/{id}")]
        public ContentResult DeleteProduct(int id)
        {
            try
            {
                var laptop = context.Laptops.FirstOrDefault(w => w.Id == id);
                context.Remove(laptop);
                context.SaveChanges();
                return Content("Product successfuly delted");
            }
            catch (Exception ex)
            {

                return Content("Error" + ex.Message);

            }
        }
    }
}