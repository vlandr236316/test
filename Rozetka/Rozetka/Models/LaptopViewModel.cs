﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Rozetka.Models
{
    public class LaptopViewModel
    {
        public string Name { get; set; }
        [Required]

        public decimal Price { get; set; }
      
        public bool IsAvailable { get; set; }
      
        public int Quantity { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

      
    }
}
