﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RozetkaUI
{
 
    public class LaptopModel
    {
     
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public string Imagge { get; set; }
        public bool IsAvailable { get; set; }
        public int Storage { get; set; }
    }
}
