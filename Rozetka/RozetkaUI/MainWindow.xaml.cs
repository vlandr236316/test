﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RozetkaUI
{
    public partial class MainWindow : Window
    {
        public List<LaptopModel> laptops;
        public MainWindow()
        {
            InitializeComponent();
            HttpWebRequest request = HttpWebRequest.CreateHttp($"http://localhost:44312/api/zakaz/getZakaz");
            request.Method = "GET";
            request.ContentType = "application/json";
            var response = request.GetResponse();
            string res = "";
            LaptopModel model;
            using (Stream stream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(stream);
                res += reader.ReadToEnd();
                model = Newtonsoft.Json.JsonConvert.DeserializeObject<LaptopModel>(res);
            }
            dataGrid.Items.Add(model);
        }

        public object JsonConvert { get; private set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            HttpWebRequest httpWebRequest = WebRequest.CreateHttp($"https://localhost:44312/api/comp/get/search/{SearchBox.Text}");
            httpWebRequest.Method = "GET";
            httpWebRequest.ContentType = "application/json";
            WebResponse web = httpWebRequest.GetResponse();
            string response = "";
            using (Stream stream = web.GetResponseStream())
            {
                StreamReader reader = new StreamReader(stream);
                response = reader.ReadToEnd();
            }




            laptops = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LaptopModel>>(response);




        }
    }
}
